package com.devcamp.j04_javabasic.s10;

import com.devcamp.j04_javabasic.s10.interfaceclass.IRunable;
import com.devcamp.j04_javabasic.s10.interfaceclass.Other;

public class CCat extends CPet implements IRunable, Other {

    public CCat(int age, String name) {
    }

    @Override
    public void other() {
        // TODO Auto-generated method stub
        System.out.println("Cat other");
    }

    @Override
    public int other(int param) {
        // TODO Auto-generated method stub
        return 5;
    }

    @Override
    public String other(String param) {
        // TODO Auto-generated method stub
        return "Cat";
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        System.out.println("Meo chay");
    }

    @Override
    public void running() {
        // TODO Auto-generated method stub
        System.out.println("Meo đang chạy");
    }

}
