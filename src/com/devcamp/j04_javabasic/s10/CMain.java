package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class CMain {
    public static void main(String[] args) {
        CPet dog = new CDog(1, "LuLU");
        CPet cat = new CCat(2, "Milo");
        ArrayList<CPet> petList = new ArrayList<>();
        petList.add(dog);
        petList.add(cat);
        CPerson person1 = new CPerson();
        CPerson person2 = new CPerson(1, 20, "le van", "thang", petList);
        System.out.println("Fullname = " + person1.getFirstName() + person1.getLastName());
        System.out.println("Fullname= " + person2.getFirstName() + person2.getLastName());
        System.out.println("age" + person2.getAge());
    }
}
