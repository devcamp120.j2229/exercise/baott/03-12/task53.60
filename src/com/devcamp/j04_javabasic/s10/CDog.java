package com.devcamp.j04_javabasic.s10;

import com.devcamp.j04_javabasic.s10.interfaceclass.IBarkable;
import com.devcamp.j04_javabasic.s10.interfaceclass.IRunable;
import com.devcamp.j04_javabasic.s10.interfaceclass.Other;

public class CDog extends CPet implements IBarkable, IRunable, Other {

    /**
     * 
     */
    public CDog(int age, String name) {
    }

    @Override
    public void other() {
        // TODO Auto-generated method stub
        System.out.println("other 1");
    }

    @Override
    public int other(int param) {
        // TODO Auto-generated method stub
        return 10;
    }

    @Override
    public String other(String param) {
        // TODO Auto-generated method stub
        return "Something";
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        System.out.println("Chó chạy");
    }

    @Override
    public void running() {
        // TODO Auto-generated method stub
        System.out.println("Chó đang chạy");
    }

    @Override
    public void bark() {
        // TODO Auto-generated method stub
        System.out.println("chó sủa");
    }

    @Override
    public void barkBanrk() {
        // TODO Auto-generated method stub

    }

    @Override
    public void animalSound() {
        this.bark();
    }

}
