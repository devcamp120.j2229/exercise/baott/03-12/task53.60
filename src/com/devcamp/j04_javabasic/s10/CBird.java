package com.devcamp.j04_javabasic.s10;

import com.devcamp.j04_javabasic.s10.interfaceclass.IFlyable;

public class CBird extends CPet implements IFlyable {

    @Override
    public void fly() {
        // TODO Auto-generated method stub
        System.out.println("Bird flying");
    }

    public static void main(String[] args) {
        CBird myBird = new CBird();
        myBird.name = "My Eagle";
        myBird.animclass = AnimalClass.birds;
        myBird.eat();
        myBird.animalSound();
        myBird.print();
        myBird.play();
        myBird.fly();

    }

}
